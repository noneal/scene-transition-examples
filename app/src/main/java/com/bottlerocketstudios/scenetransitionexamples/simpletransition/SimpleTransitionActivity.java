package com.bottlerocketstudios.scenetransitionexamples.simpletransition;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.bottlerocketstudios.scenetransitionexamples.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathan.oneal on 9/13/16.
 */

public class SimpleTransitionActivity extends AppCompatActivity {
    private static final String TAG = SimpleTransitionActivity.class.getSimpleName();

    private static final int TOP_LEFT = 1;
    private static final int TOP_RIGHT = 2;
    private static final int BOTTOM_RIGHT = 3;
    private static final int BOTTOM_LEFT = 4;

    private List<Button> mButtons;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_transition);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final CheckBox useTransitionCheckbox = (CheckBox) findViewById(R.id.use_transition_checkbox);

        mButtons = new ArrayList<>();
        mButtons.add((Button) findViewById(R.id.button_clockwise));
        mButtons.add((Button) findViewById(R.id.button_counter_clockwise));
        mButtons.add((Button) findViewById(R.id.button_hide));
        mButtons.add((Button) findViewById(R.id.button_show));

        //Simple Transition for Clockwise movement
        mButtons.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (useTransitionCheckbox.isChecked()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        TransitionManager.beginDelayedTransition((RelativeLayout) findViewById(R.id.root_view));
                    }
                }
                rotateButtons(true);
            }
        });

        //Use TransitionSet to stagger Counter Clockwise movement
        mButtons.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (useTransitionCheckbox.isChecked()) {
                    TransitionSet transitionSet = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        transitionSet = new TransitionSet();

                        Transition first = new ChangeBounds();
                        Transition second = new ChangeBounds();
                        Transition third = new ChangeBounds();
                        Transition fourth = new ChangeBounds();

                        for (Button button : mButtons) {

                            if (getPosition(button) == TOP_LEFT) {
                                first.addTarget(button);
                            } else if (getPosition(button) == BOTTOM_LEFT) {
                                second.addTarget(button).setStartDelay(150);
                            } else if (getPosition(button) == BOTTOM_RIGHT) {
                                third.addTarget(button).setStartDelay(300);
                            } else {
                                fourth.addTarget(button).setStartDelay(450);
                            }
                        }

                        transitionSet.addTransition(first);
                        transitionSet.addTransition(second);
                        transitionSet.addTransition(third);
                        transitionSet.addTransition(fourth);

                        transitionSet.setDuration(500);

                        TransitionManager.beginDelayedTransition((RelativeLayout) findViewById(R.id.root_view), transitionSet);
                    }
                }

                rotateButtons(false);
            }
        });

        //Hide Buttons
        mButtons.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (useTransitionCheckbox.isChecked()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        TransitionManager.beginDelayedTransition((RelativeLayout) findViewById(R.id.root_view));
                    }
                }

                hide();
            }
        });

        //Show Buttons
        mButtons.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (useTransitionCheckbox.isChecked()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        TransitionManager.beginDelayedTransition((RelativeLayout) findViewById(R.id.root_view));
                    }
                }

                show();
            }
        });
    }

    private void hide() {

        for (Button button : mButtons) {
            if (button.getId() == R.id.button_clockwise || button.getId() == R.id.button_counter_clockwise) {
                button.setVisibility(View.GONE);
            }
        }
    }

    private void show() {

        for (Button button : mButtons) {
            if (button.getId() == R.id.button_clockwise || button.getId() == R.id.button_counter_clockwise) {
                button.setVisibility(View.VISIBLE);
            }
        }
    }

    private void rotateButtons(boolean clockwise) {

        for (Button button : mButtons) {

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) button.getLayoutParams();

            int buttonPosition = getPosition(lp);

            //Button is on top left
            if (buttonPosition == TOP_LEFT) {
                if (clockwise) {
                    //move to top right
                    moveRight(lp);
                } else {
                    //move to bottom left
                    moveDown(lp);
                }
            }
            //Button is on top right
            else if (buttonPosition == TOP_RIGHT) {
                if (clockwise) {
                    //move to bottom right
                    moveDown(lp);
                } else {
                    //move to top left
                    moveLeft(lp);
                }
            }
            //Button is on bottom right
            else if (buttonPosition == BOTTOM_RIGHT) {
                if (clockwise) {
                    //move to bottom left
                    moveLeft(lp);
                } else {
                    //move to top right
                    moveUp(lp);
                }
            }
            //Button is on bottom left
            else {
                if (clockwise) {
                    //move to top left
                    moveUp(lp);
                } else {
                    //move to bottom right
                    moveRight(lp);
                }
            }

            button.setLayoutParams(lp);
        }
    }

    private int getPosition(Button button) {
        return getPosition((RelativeLayout.LayoutParams) button.getLayoutParams());
    }

    private int getPosition(RelativeLayout.LayoutParams lp) {

        //Button is on top left
        if (lp.getRules()[RelativeLayout.ALIGN_PARENT_TOP] != 0 && lp.getRules()[RelativeLayout.ALIGN_PARENT_LEFT] != 0) {
            return TOP_LEFT;
        }
        //Button is on top right
        else if (lp.getRules()[RelativeLayout.ALIGN_PARENT_TOP] != 0 && lp.getRules()[RelativeLayout.ALIGN_PARENT_RIGHT] != 0) {
            return TOP_RIGHT;
        }
        //Button is on bottom right
        else if (lp.getRules()[RelativeLayout.ALIGN_PARENT_BOTTOM] != 0 && lp.getRules()[RelativeLayout.ALIGN_PARENT_RIGHT] != 0) {
            return BOTTOM_RIGHT;
        }
        //Button is on bottom left
        else {
            return BOTTOM_LEFT;
        }
    }

    private void moveRight(RelativeLayout.LayoutParams layoutParams) {
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 1);
    }

    private void moveLeft(RelativeLayout.LayoutParams layoutParams) {
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 1);
    }

    private void moveUp(RelativeLayout.LayoutParams layoutParams) {
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);
    }

    private void moveDown(RelativeLayout.LayoutParams layoutParams) {
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
    }
}