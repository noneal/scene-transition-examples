package com.bottlerocketstudios.scenetransitionexamples.activitytransition;

import com.bottlerocketstudios.scenetransitionexamples.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathan.oneal on 9/30/16.
 */

public class AlbumData {

    public static List<Album> sAlbumList;

    public static List<Album> getAlbums() {

        if (sAlbumList == null) {

            sAlbumList = new ArrayList<>();

            sAlbumList.add(new Album(R.drawable.animals));
            sAlbumList.add(new Album(R.drawable.joshua_tree));
            sAlbumList.add(new Album(R.drawable.heaven));
            sAlbumList.add(new Album(R.drawable.reading));
            sAlbumList.add(new Album(R.drawable.wish_you_were_here));
            sAlbumList.add(new Album(R.drawable.substance));
            sAlbumList.add(new Album(R.drawable.the_wall));
            sAlbumList.add(new Album(R.drawable.songs));
            sAlbumList.add(new Album(R.drawable.unforgettable_fire));
            sAlbumList.add(new Album(R.drawable.music));
            sAlbumList.add(new Album(R.drawable.power));
            sAlbumList.add(new Album(R.drawable.brightly));
        }

        return sAlbumList;
    }
}
