package com.bottlerocketstudios.scenetransitionexamples.activitytransition;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.bottlerocketstudios.scenetransitionexamples.R;

/**
 * Created by nathan.oneal on 9/13/16.
 */

public class ActivityTransitionDetailsActivity extends AppCompatActivity {
    private static final String TAG = ActivityTransitionDetailsActivity.class.getSimpleName();

    public static final String EXTRA_ALBUM_POSITION = "extraAlbumPosition";

    private int mCurrentPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mCurrentPosition = getIntent().getIntExtra(EXTRA_ALBUM_POSITION, 0);

        // Delay Entry transition until NavBar is ready to be drawn
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
        }

        final View decorView = getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                decorView.getViewTreeObserver().removeOnPreDrawListener(this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startPostponedEnterTransition();
                }
                return true;
            }
        });

        ImageView albumImage = (ImageView) findViewById(R.id.album_cover);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            albumImage.setTransitionName(getString(R.string.album_transition, Integer.toString(mCurrentPosition)));

            //set enter animation for content
            Slide slide = new Slide(Gravity.BOTTOM);
            slide.excludeTarget(android.R.id.statusBarBackground, true);
            slide.excludeTarget(android.R.id.navigationBarBackground, true);

            View decor = getWindow().getDecorView();
            slide.excludeTarget(decor.findViewById(R.id.action_bar_container), true);

            getWindow().setEnterTransition(slide);
        }
        albumImage.setImageResource(AlbumData.getAlbums().get(mCurrentPosition).getImageResId());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    setActivityResult();
                    //ensure transition is run when using up navigation
                    finishAfterTransition();
                    return true;
                }

        }
        return super.onOptionsItemSelected(item);
    }

    private void setActivityResult() {
        Intent intent = new Intent();

        //pass back position so recyclerView can scroll to correct position
        intent.putExtra(EXTRA_ALBUM_POSITION, mCurrentPosition);

        //set result to ensure
        setResult(RESULT_OK, intent);
    }

    @Override
    public void onBackPressed() {
        setActivityResult();
        super.onBackPressed();
    }
}