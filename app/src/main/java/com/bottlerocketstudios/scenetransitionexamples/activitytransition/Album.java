package com.bottlerocketstudios.scenetransitionexamples.activitytransition;

/**
 * Created by nathan.oneal on 9/30/16.
 */

public class Album {
    private int mImageResId;

    public Album(int imageResId) {
        mImageResId = imageResId;
    }

    public int getImageResId() {
        return mImageResId;
    }
}