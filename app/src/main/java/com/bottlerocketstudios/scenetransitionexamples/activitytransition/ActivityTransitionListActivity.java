package com.bottlerocketstudios.scenetransitionexamples.activitytransition;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;

import com.bottlerocketstudios.scenetransitionexamples.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathan.oneal on 9/13/16.
 */

public class ActivityTransitionListActivity extends AppCompatActivity {
    private static final String TAG = ActivityTransitionListActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transition_list);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //set Exit Transition as an Explode
            Explode explode = new Explode();

            explode.excludeTarget(android.R.id.statusBarBackground, true);

            View decor = getWindow().getDecorView();
            explode.excludeTarget(decor.findViewById(R.id.action_bar_container), true);

            getWindow().setExitTransition(explode);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        int span = 2;
        if (getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
            span = 4;
        }

        GridLayoutManager layoutManager = new GridLayoutManager(this, span);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        AlbumAdapter adapter = new AlbumAdapter(AlbumData.getAlbums());
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityReenter(int resultCode, Intent data) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
        }

        final int position = data.getIntExtra(ActivityTransitionDetailsActivity.EXTRA_ALBUM_POSITION, 0);

        mRecyclerView.scrollToPosition(position);

        // Start the postponed transition when the recycler view is ready to be drawn.
        mRecyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startPostponedEnterTransition();
                }
                return true;
            }
        });
    }

    private class AlbumAdapter extends RecyclerView.Adapter<AlbumViewHolder> {

        private List<Album> mAlbumList;

        public AlbumAdapter(List<Album> albumList) {
            mAlbumList = albumList;
        }

        @Override
        public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_transition_list_item, parent, false);

            return new AlbumViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return mAlbumList.size();
        }

        @Override
        public void onBindViewHolder(final AlbumViewHolder holder, int position) {
            Album album = mAlbumList.get(position);

            holder.mAlbumImage.setImageResource(album.getImageResId());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.mAlbumImage.setTransitionName(getString(R.string.album_transition, Integer.toString(position)));
            }

            holder.mAlbumImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(ActivityTransitionListActivity.this, ActivityTransitionDetailsActivity.class);
                    intent.putExtra(ActivityTransitionDetailsActivity.EXTRA_ALBUM_POSITION, holder.getAdapterPosition());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                        List<Pair<View, String>> pairs = new ArrayList<>();

                        //add image view from Destination Activity as shared element
                        pairs.add(Pair.create(view, view.getTransitionName()));
                        //add Nav Bar as shared element to prevent album imageView overlap
                        pairs.add(Pair.create(findViewById(android.R.id.navigationBarBackground), Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME));

                        ActivityOptionsCompat activityOptions =
                                ActivityOptionsCompat.makeSceneTransitionAnimation(ActivityTransitionListActivity.this, pairs.toArray(new Pair[pairs.size()]));

                        ActivityCompat.startActivity(ActivityTransitionListActivity.this, intent, activityOptions.toBundle());
                    } else {
                        startActivity(intent);
                    }
                }
            });
        }
    }

    private class AlbumViewHolder extends RecyclerView.ViewHolder {

        private ImageView mAlbumImage;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            mAlbumImage = (ImageView) itemView.findViewById(R.id.album_cover);
        }
    }

    private int getScreenOrientation()
    {
        Display display = getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_PORTRAIT;
        Point screentSize = new Point();
        display.getSize(screentSize);

        if(screentSize.x > screentSize.y){
            orientation = Configuration.ORIENTATION_LANDSCAPE;
        }

        return orientation;
    }
}