package com.bottlerocketstudios.scenetransitionexamples.fragmenttransition;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.Fade;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bottlerocketstudios.scenetransitionexamples.R;

/**
 * Created by nathan.oneal on 10/2/16.
 */

public class FragmentOne extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setExitTransition(new Fade());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_transition_one, container, false);

        final ImageView albumImageView = (ImageView) view.findViewById(R.id.album_image);

        albumImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Create new fragment
                        Fragment fragment = new FragmentTwo();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            fragment.setSharedElementEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.image_transition));
                        }

                        FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.root_view, fragment)
                                .addToBackStack("transaction");


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ft.addSharedElement(albumImageView, albumImageView.getTransitionName());
                        }

                        ft.commit();
                    }
                });

        return view;
    }
}
